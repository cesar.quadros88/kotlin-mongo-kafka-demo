package com.example.demo.api

import com.example.demo.domain.People
import com.example.demo.domain.service.PeopleService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/people")
class PeopleController (private val service: PeopleService){

    @PostMapping
    fun create(@RequestBody people: People) : ResponseEntity<People> = ResponseEntity.ok(service.create(people))

    @GetMapping
    fun findAll() : ResponseEntity<List<People>> = ResponseEntity.ok(service.findAll())
}