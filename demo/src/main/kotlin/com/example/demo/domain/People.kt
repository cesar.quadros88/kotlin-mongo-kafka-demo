package com.example.demo.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class People(
    @Id
    val id: String? = null,
    var name: String,
    var email: String
)
