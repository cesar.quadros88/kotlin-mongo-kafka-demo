package com.example.demo.domain.service

import com.example.demo.domain.People
import com.example.demo.infra.KafkaProducer
import com.example.demo.infra.PeopleRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Service

@Service
class PeopleService(private val repository: PeopleRepository, private val producer: KafkaProducer, private val mapper: ObjectMapper) {

    fun create(people: People) : People {
        val savePeople = this.repository.save(people)
        this.producer.sendMessage(mapper.writeValueAsString(savePeople))
        return savePeople
    }

    fun findAll(): List<People> = this.repository.findAll()
}