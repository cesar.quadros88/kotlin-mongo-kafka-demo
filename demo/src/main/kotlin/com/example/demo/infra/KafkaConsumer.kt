package com.example.demo.infra

import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class KafkaConsumer {

    private val logger = LoggerFactory.getLogger(javaClass)

    @KafkaListener(topics = ["mail-topic"], groupId = "mail-group")
    fun processMessage(message: String){
        logger.info("message received: [$message]")
    }
}