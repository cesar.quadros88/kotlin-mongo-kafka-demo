package com.example.demo.infra

import org.slf4j.LoggerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class KafkaProducer (private val kafkaTemplate: KafkaTemplate<String, String>){

    private val logger = LoggerFactory.getLogger(javaClass)

    fun sendMessage(message: String) {
        logger.info("send message [$message] to topic mail-topic")
        kafkaTemplate.send("mail-topic", message)
    }
}