package com.example.demo.infra

import com.example.demo.domain.People
import org.springframework.data.mongodb.repository.MongoRepository

interface PeopleRepository : MongoRepository<People, String> {
}